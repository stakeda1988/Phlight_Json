//
//  TableViewController.swift
//  Kadonashi
//
//  Created by SHOKI TAKEDA on 12/5/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class TableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, NADViewDelegate {
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBAction func tapView(sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        tableView.endEditing(true)
    }
    var filteredTableData = [String]()
    var tableData = [String]()
//    var resultSearchController = UISearchController()
    var resourceArray:[[String]] = []
    var json:JSON?
    var searchActive : Bool = false
    var infoArray = [String]()
    var selectedText:String?
    var resultText:String?
    var sentenceText:String?
    var AdView:UIView?
    var CellAdView:UIView?
    var adTimer:NSTimer!
    let defaults = NSUserDefaults.standardUserDefaults()
    private var nadView: NADView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        
        adTimer = NSTimer.scheduledTimerWithTimeInterval(10.0, target: self, selector: Selector("onAdStart"), userInfo: nil, repeats: true)
        
        let jsonUrl:String = "http://phlight.herokuapp.com/api/items/"
        if(CheckReachability(jsonUrl)) {
            bottomConstraint.constant = 50
            var appDomain:String = NSBundle.mainBundle().bundleIdentifier!
            NSUserDefaults.standardUserDefaults().removePersistentDomainForName(appDomain)
            json = JSON(url:jsonUrl)
            print(json)
            var count:Int = 0
            for (i,prefecture) in json! {
                for (key, value) in prefecture {
                    if key as! String == "name" {
                        tableData.append(String(value))
                    } else if key as! String == "info" {
                        infoArray.append(String(value))
                        count++
                    }
                }
            }
            defaults.setObject(tableData, forKey:"tableData")
            defaults.setObject(infoArray, forKey:"infoArray")
            defaults.synchronize()
        } else {
            tableData = []
            infoArray = []
            
            if((defaults.objectForKey("tableData")) != nil){
                let objects = defaults.objectForKey("tableData") as? NSArray
                var nameString:AnyObject
                for nameString in objects!{
                    tableData.append(nameString as! String)
                }
            }
            if((defaults.objectForKey("infoArray")) != nil){
                let objects = defaults.objectForKey("infoArray") as? NSArray
                var nameString:AnyObject
                for nameString in objects!{
                    infoArray.append(nameString as! String)
                }
            }
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        tableView.backgroundView = nil
        tableView.estimatedRowHeight = 90
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.backgroundColor = UIColor.clearColor()
        tableView.separatorColor = UIColor.clearColor()
//        let imageView = UIImageView(frame: CGRectMake(0, 0, myBoundSize.width, myBoundSize.height))
//        let image = UIImage(named: "backView")
//        imageView.image = image
//        imageView.alpha = 1.0
//        tableView.backgroundView = imageView
        
//        self.resultSearchController = ({
//            let controller = UISearchController(searchResultsController: nil)
//            controller.searchResultsUpdater = self
//            controller.dimsBackgroundDuringPresentation = false
//            controller.searchBar.sizeToFit()
//            
//            self.tableView.tableHeaderView = controller.searchBar
//            
//            return controller
//        })()
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (searchActive) {
            return self.filteredTableData.count
        }
        else {
            return tableData.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "Cell")
        cell.backgroundColor = UIColor.clearColor()
        let cellSelectedBgView = UIView()
        cellSelectedBgView.backgroundColor = UIColor.clearColor()
        cell.selectedBackgroundView = cellSelectedBgView
        cell.contentView.backgroundColor = UIColor.clearColor()
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.font = UIFont(name: "GurmukhiMN", size: 15)
        cell.textLabel?.textColor = UIColor.whiteColor()
        if (searchActive) {
            filteredTableData = filteredTableData.reverse()
            cell.textLabel?.text = filteredTableData[indexPath.row]
            return cell
        }
        else {
//            tableData.sortInPlace { $0 < $1 }
            tableData = tableData.reverse()
            cell.textLabel?.text = tableData[indexPath.row]
            return cell
        }
    }
    
    func tableView(table: UITableView, didSelectRowAtIndexPath indexPath:NSIndexPath) {
        let cell:UITableViewCell = self.tableView.cellForRowAtIndexPath(indexPath)!
        selectedText = (cell.textLabel?.text)!
        for i in 0...tableData.count {
            if selectedText == tableData[i]{
                sentenceText = selectedText
                resultText = infoArray[i]
                break
            }
        }
        performSegueWithIdentifier("shift",sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "shift" {
            let resultVC:ResultViewController = segue.destinationViewController as! ResultViewController
            resultVC.sentenceLabel = sentenceText!
            resultVC.topicLabel = resultText!
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
//        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
//        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
//        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.view.endEditing(true)
//        searchActive = false;
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        filteredTableData = tableData.filter({ (text) -> Bool in
            let tmp: NSString = text
            let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
            return range.location != NSNotFound
        })
        if(filteredTableData.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tableView.reloadData()
    }
    
    func onAdStart() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        nadView.removeFromSuperview()
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
}