//
//  ResultViewController.swift
//  Kadonashi
//
//  Created by SHOKI TAKEDA on 12/5/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit
import Social
import MessageUI

class ResultViewController: UIViewController, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate, NADViewDelegate {
    @IBOutlet weak var resultBottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var mainLabelWidth: NSLayoutConstraint!
    @IBOutlet weak var mailBtn: UIButton!
    
    @IBOutlet weak var twitterBtn: UIButton!
    @IBOutlet weak var fbBtn: UIButton!
    
    @IBAction func twitter(sender: UIButton) {
        let alertController = UIAlertController(title: "Confirmation", message: "Post to Twitter?", preferredStyle: .Alert)
        let otherAction = UIAlertAction(title: "OK", style: .Default) {
            action in
            let controller = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            let title: String = self.sentenceLabel + " " + self.topicLabel + " #Phlight"
            controller.setInitialText(title)
            self.presentViewController(controller, animated: true, completion: {})
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: .Cancel) {
            action in self.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController.addAction(otherAction)
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func facebook(sender: UIButton) {
        let alertController = UIAlertController(title: "Confirmation", message: "Post to Facebok?", preferredStyle: .Alert)
        let otherAction = UIAlertAction(title: "OK", style: .Default) {
            action in
            let controller = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            let title: String = self.sentenceLabel + " " + self.topicLabel + " #Phlight"
            controller.setInitialText(title)
            self.presentViewController(controller, animated: true, completion: {})
        }
        let cancelAction = UIAlertAction(title: "CANCEL", style: .Cancel) {
            action in self.dismissViewControllerAnimated(true, completion: nil)
        }
        alertController.addAction(otherAction)
        alertController.addAction(cancelAction)
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func mailUp(sender: UIButton) {
        let mailViewController = MFMailComposeViewController()
        let toRecipients = ["testflights2015@gmail.com"]
        mailViewController.mailComposeDelegate = self
        mailViewController.setSubject("Feedback")
        mailViewController.setToRecipients(toRecipients)
        mailViewController.setMessageBody("", isHTML: false)
        self.presentViewController(mailViewController, animated: true, completion: nil)
        
    }
    @IBOutlet weak var mainLabel: UILabel!
    var sentenceLabel:String = ""
    var topicLabel:String = ""
    private var nadView: NADView!
    var timer:NSTimer!
    override func viewDidLoad() {
        super.viewDidLoad()
        let jsonUrl:String = "http://phlight.herokuapp.com/api/items/"
        if(CheckReachability(jsonUrl)) {
        } else{
            resultBottomConstraints.constant = 0
        }
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        mainLabelWidth.constant = myBoundSize.width - 20
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
        
        twitterBtn.backgroundColor = UIColor.clearColor()
        twitterBtn.setImage(UIImage(named: "twitter-big.jpg"), forState: .Normal)
        twitterBtn.imageView!.contentMode = .ScaleAspectFit
        
        fbBtn.backgroundColor = UIColor.clearColor()
        fbBtn.setImage(UIImage(named: "facebook-big.jpg"), forState: .Normal)
        fbBtn.imageView!.contentMode = .ScaleAspectFit
        
        mainLabelWidth.constant = myBoundSize.width - 20
        mainLabel?.font = UIFont(name: "GurmukhiMN", size: 15)
        mailBtn.titleLabel?.font = UIFont(name: "GurmukhiMN", size: 15)
        mainLabel.baselineAdjustment = UIBaselineAdjustment.AlignCenters
        mainLabel.text = sentenceLabel + "\n\n" + topicLabel
        
        timer = NSTimer.scheduledTimerWithTimeInterval(10.0, target: self, selector: Selector("onAdUpdate"), userInfo: nil, repeats: true)
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func onAdUpdate() {
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        mainLabelWidth.constant = myBoundSize.width - 20
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func viewDidAppear(animated: Bool) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onOrientationChange:", name: UIDeviceOrientationDidChangeNotification, object: nil)
    }
    
    // 端末の向きがかわったら呼び出される.
    func onOrientationChange(notification: NSNotification){
        nadView.removeFromSuperview()
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        mainLabelWidth.constant = myBoundSize.width - 20
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-50, width: 320, height: 50))
        nadView.setNendID("3df85edb7a0b0b30ed826a296a0c0881a8612fe1",
            spotID: "492647")
        nadView.isOutputLog = false
        nadView.delegate = self
        nadView.load()
        self.view.addSubview(nadView)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}