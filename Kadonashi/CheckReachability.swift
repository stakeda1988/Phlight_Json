//
//  CheckReachability.swift
//  Sacuri 開発日誌
//  http://press.treastrain.jp/examine-whether-able-to-network-access-swift2/
//
//  Created by treastrain / Tanaka Ryoga on 2015/08/19.
//  Copyright © 2015年 treastrain / Tanaka Ryoga. All rights reserved.
//

import SystemConfiguration

func CheckReachability(host_name:String)->Bool{
    
    let reachability = SCNetworkReachabilityCreateWithName(nil, host_name)!
    var flags = SCNetworkReachabilityFlags.ConnectionAutomatic
    if !SCNetworkReachabilityGetFlags(reachability, &flags) {
        return false
    }
    let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
    let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
    return (isReachable && !needsConnection)
}